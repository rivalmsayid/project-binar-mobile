package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable
import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

public class add_new_product {
	@Given("user in account page")
	public void user_in_account_page() {
		Mobile.verifyElementVisible(findTestObject('Object Repository/my_account/title_akun_saya'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/my_account/button_edit_profile'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/my_account/button_daftar_jual_saya'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/my_account/button_pesanan_saya'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/my_account/button_keluar'), 0)
	}

	@And("user tap jual button")
	public void user_tap_jual_button() {
		Mobile.tap(findTestObject('Object Repository/homepage/button_add_product'), 0)
	}

	@And("user redirect to add product page")
	public void user_redirect_to_add_product_page() {
		Mobile.verifyElementVisible(findTestObject('Object Repository/add_product/input_nama_produk'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/add_product/input_harga_produk'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/add_product/dropdown_kategori_produk'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/add_product/input_lokasi_produk'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/add_product/input_deskripsi_produk'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/add_product/button_foto_produk'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/add_product/button_preview'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/add_product/button_terbitkan'), 0)
	}

	@When("user is input all the product data fields")
	public void user_is_input_product_data() {
		Mobile.setText(findTestObject('Object Repository/add_product/input_nama_produk'), 'Samsung Galaxy Flip', 0)
		Mobile.setText(findTestObject('Object Repository/add_product/input_harga_produk'), '10000000', 0)
		Mobile.tap(findTestObject('Object Repository/add_product/dropdown_kategori_produk'), 0)
		Mobile.tap(findTestObject('Object Repository/add_product/kategori_1'), 0)
		Mobile.setText(findTestObject('Object Repository/add_product/input_lokasi_produk'), 'Jakarta Selatan', 0)
		Mobile.setText(findTestObject('Object Repository/add_product/input_deskripsi_produk'), 'Samsung Galaxy Flip Second Mulus Like New', 0)
		Mobile.tap(findTestObject('Object Repository/add_product/button_foto_produk'), 0)
		Mobile.tap(findTestObject('Object Repository/add_product/button_to_gallery'), 0)
		Mobile.tap(findTestObject('Object Repository/add_product/product_image1'), 0)
	}

	@When("user is input the product data except (.*) field")
	public void user_is_input_product_data_except_field(String condition) {
		if(condition=="Name") {
			Mobile.setText(findTestObject('Object Repository/add_product/input_harga_produk'), '10000000', 0)
			Mobile.tap(findTestObject('Object Repository/add_product/dropdown_kategori_produk'), 0)
			Mobile.tap(findTestObject('Object Repository/add_product/kategori_1'), 0)
			Mobile.setText(findTestObject('Object Repository/add_product/input_lokasi_produk'), 'Jakarta Selatan', 0)
			Mobile.setText(findTestObject('Object Repository/add_product/input_deskripsi_produk'), 'Samsung Galaxy Flip Second Mulus Like New', 0)
			Mobile.tap(findTestObject('Object Repository/add_product/button_foto_produk'), 0)
			Mobile.tap(findTestObject('Object Repository/add_product/button_to_gallery'), 0)
			Mobile.tap(findTestObject('Object Repository/add_product/product_image1'), 0)
		}else if(condition=="Price") {
			Mobile.setText(findTestObject('Object Repository/add_product/input_nama_produk'), 'Samsung Galaxy Flip', 0)
			Mobile.tap(findTestObject('Object Repository/add_product/dropdown_kategori_produk'), 0)
			Mobile.tap(findTestObject('Object Repository/add_product/kategori_1'), 0)
			Mobile.setText(findTestObject('Object Repository/add_product/input_lokasi_produk'), 'Jakarta Selatan', 0)
			Mobile.setText(findTestObject('Object Repository/add_product/input_deskripsi_produk'), 'Samsung Galaxy Flip Second Mulus Like New', 0)
			Mobile.tap(findTestObject('Object Repository/add_product/button_foto_produk'), 0)
			Mobile.tap(findTestObject('Object Repository/add_product/button_to_gallery'), 0)
			Mobile.tap(findTestObject('Object Repository/add_product/product_image1'), 0)
		}
	}

	@And("user tap terbitkan button")
	public void user_tap_terbitkan_button() {
		Mobile.tap(findTestObject('Object Repository/add_product/button_terbitkan'), 0)
	}

	@Then("user successfully add new product")
	public void user_successfully_add_new_product() {
		Mobile.verifyElementVisible(findTestObject('Object Repository/daftar_jual_saya/product_card'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/daftar_jual_saya/button_delete_product1'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/daftar_jual_saya/button_produk'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/daftar_jual_saya/button_diminati'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/daftar_jual_saya/button_terjual'), 0)
	}

	@Then("user will not sucessfully add the product and see (.*)")
	public void user_will_not_sucessfully_add_the_product_and_see(String message) {
		if(message=="Nama Produk tidak boleh kosong") {
			Mobile.verifyElementVisible(findTestObject('Object Repository/add_product/input_nama_produk'), 0)
			Mobile.verifyElementVisible(findTestObject('Object Repository/add_product/message_name_blank'), 0)
		}else if(message=="Harga Produk tidak boleh kosong") {
			Mobile.verifyElementVisible(findTestObject('Object Repository/add_product/input_harga_produk'), 0)
			Mobile.verifyElementVisible(findTestObject('Object Repository/add_product/message_price_blank'), 0)
		}
	}
}
