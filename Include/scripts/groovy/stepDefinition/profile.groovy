package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import java.security.PublicKey

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.mobile.keyword.internal.MobileAbstractKeyword
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import groovy.sql.ResultSetMetaDataWrapper
import internal.GlobalVariable

public class profile {
	@Given("user click edit button")
	public void user_click_edit_button() {
		Mobile.tap(findTestObject('Object Repository/my_account/button_edit_profile'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/profile/button_image'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/profile/info_profil'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/profile/info_akun'), 0)
	}

	@When("user click image button")
	public void user_click_image_button() {
		Mobile.tap(findTestObject('Object Repository/profile/button_image'), 0)
	}

	@When("user click galerry button")
	public void user_click_galerry_button() {
		Mobile.tap(findTestObject('Object Repository/profile/button_galeri'), 0)
	}

	@When("user select image")
	public void user_select_image() {
		Mobile.tap(findTestObject('Object Repository/profile/profile_image'), 0)
	}

	@Then("user get pop-up message profil berhasil diperbaharui")
	public void user_get_pop_up_message_profil_berhasil_diperbaharui() {
		Mobile.verifyElementVisible(findTestObject('Object Repository/profile/button_image'), 0)
	}

	@Given("user click (.*) field")
	public void user_click_field(String field) {

		if(field=="nama") {
			Mobile.tap(findTestObject('Object Repository/profile/nama'), 0)
		}else if(field=="nomor hp") {
			Mobile.tap(findTestObject('Object Repository/profile/nomor_hp'), 0)
		}else if(field=="alamat") {
			Mobile.tap(findTestObject('Object Repository/profile/alamat'), 0)
		}
	}

	@And("user update profile but (.*)")
	public void user_update_profile_but(String condition) {

		if(condition=="leave nama field empty") {
			Mobile.clearText(findTestObject('Object Repository/profile/input_nama'), 0)
			Mobile.delay(3)
		}else if(condition=="leave nomor hp field empty") {
			Mobile.clearText(findTestObject('Object Repository/profile/input_nomor_hp'), 0)
			Mobile.delay(3)
		}else if(condition=="input spaces in alamat field") {
			Mobile.clearText(findTestObject('Object Repository/profile/input_alamat'), 0)
			Mobile.setText(findTestObject('Object Repository/profile/input_alamat'), '   ', 0)
		}
	}

	@When("user click simpan button")
	public void user_click_simpan_button() {
		Mobile.tap(findTestObject('Object Repository/profile/button_simpan'), 0)
	}

	@Then("user get pop-up message error (.*)")
	public void user_get_pop_up_message_error(String result) {

		if(result=='wajib diisi') {
			Mobile.verifyElementVisible(findTestObject('Object Repository/profile/input_nama'), 0)
			Mobile.verifyElementVisible(findTestObject('Object Repository/profile/button_simpan'), 0)
			Mobile.verifyElementVisible(findTestObject('Object Repository/profile/pop_up_error_wajib_diisi'), 0)
		}else if(result=='wajib diisi') {
			Mobile.verifyElementVisible(findTestObject('Object Repository/profile/input_nomor_hp'), 0)
			Mobile.verifyElementVisible(findTestObject('Object Repository/profile/button_simpan'), 0)
			Mobile.verifyElementVisible(findTestObject('Object Repository/profile/pop_up_error_wajib_diisi'), 0)
		}else if(result=='wajib diisi') {
			Mobile.verifyElementVisible(findTestObject('Object Repository/profile/alamat'), 0)
			Mobile.verifyElementVisible(findTestObject('Object Repository/profile/button_simpan'), 0)
			Mobile.verifyElementVisible(findTestObject('Object Repository/profile/pop_up_error_wajib_diisi'), 0)
		}
	}
}

