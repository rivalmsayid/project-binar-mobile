package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import cucumber.api.java.en.Given
import cucumber.api.java.en.When
import cucumber.api.java.en.Then
import internal.GlobalVariable

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import io.appium.java_client.AppiumDriver

import com.kms.katalon.core.util.KeywordUtil



public class sell_product {
	@Given('user login as seller')
	public void user_login_as_seller() {
		Mobile.startApplication('Application/secondhand-24082023.apk', true)
		Mobile.tap(findTestObject('Object Repository/homepage/button_akun'), 0)
		Mobile.tap(findTestObject('Object Repository/my_account/button_masuk'), 0)
		Mobile.setText(findTestObject('Object Repository/login/input_email'), 'anggasaputra.sg@gmail.com', 0)
		Mobile.setText(findTestObject('Object Repository/login/input_password'), 'binaracademy', 0)
		Mobile.tap(findTestObject('Object Repository/login/button_masuk'), 0)
	}

	@When('user view the notification')
	public void user_view_the_notification() {
		Mobile.tap(findTestObject('Object Repository/my_account/button_daftar_jual_saya'), 0)
		Mobile.tap(findTestObject('Object Repository/sell_product/page_diminati'), 0)
	}

	@When('user choose product offer')
	public void user_choose_product_offer() {
		Mobile.tap(findTestObject('Object Repository/sell_product/product_diminati'), 0)
	}

	@When('user accept the offer from buyer')
	public void user_accept_offer() {
		Mobile.tap(findTestObject('Object Repository/sell_product/button_terima_penawaran'), 0)
		Mobile.delay(3)
	}

	@Then('user stay on the offer page')
	public void user_stay_on_the_offer_page() {
		Mobile.verifyElementVisible(findTestObject('Object Repository/sell_product/button_hubungi_whatsapp'), 0)
	}

	@Given('user go to home page')
	public void user_go_to_homepage() {
		Mobile.startApplication('Application/secondhand-24082023.apk', true)
		Mobile.verifyElementVisible(findTestObject('Object Repository/buy_product/card_product_view'), 0)
	}

	@When('user click the sell button')
	public void user_click_the_sell_button() {
		Mobile.tap(findTestObject('Object Repository/sell_product/button_sell'), 0)
	}

	@Then('user redirected to login page that user must login first')
	public void user_redirected_to_login_page() {
		Mobile.verifyElementVisible(findTestObject('Object Repository/sell_product/button_masuk_add_product'), 0)
		Mobile.closeApplication()
	}
}
