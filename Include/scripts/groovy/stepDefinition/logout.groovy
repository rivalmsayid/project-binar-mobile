package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.mobile.keyword.internal.MobileAbstractKeyword
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows


import cucumber.api.java.en.Given
import cucumber.api.java.en.When
import cucumber.api.java.en.Then
import cucumber.api.java.en.And
import internal.GlobalVariable


public class logout {
	@When("user click keluar button")
	public void user_click_keluar_button() {
		Mobile.verifyElementVisible(findTestObject('Object Repository/my_account/button_daftar_jual_saya'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/my_account/button_pesanan_saya'), 0)
		Mobile.tap(findTestObject('Object Repository/my_account/button_keluar'), 0)
	}

	@Then("user successfully log out")
	public void user_successfully_log_out() {
		Mobile.verifyElementVisible(findTestObject('Object Repository/homepage/button_beranda'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/homepage/button_transaksi'), 0)
		Mobile.delay(3)
		Mobile.closeApplication()
	}
}
