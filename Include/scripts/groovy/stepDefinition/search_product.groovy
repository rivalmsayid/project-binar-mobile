package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import java.security.PublicKey

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable
import cucumber.api.java.en.Given
import cucumber.api.java.en.When
import cucumber.api.java.en.Then
import org.openqa.selenium.Keys as Keys

public class search_product {
	@When("user tap search field")
	public void user_tap_search_field () {
		Mobile.tap(findTestObject('Object Repository/homepage/search_field'), 0)
		Mobile.delay(3)
	}

	@When("user inputs the keyword buku in the search field")
	public void user_inputs_the_keyword_buku_in_the_search_field() {
		Mobile.tap(findTestObject('Object Repository/search_product_page/search field page'), 0)
		Mobile.setText(findTestObject('Object Repository/search_product_page/search field page'), 'buku', 0)
	}

	@Then("the system will display a list of products with name buku")
	public void the_system_will_display_a_list_of_products_with_name_buku() {
		Mobile.verifyElementVisible(findTestObject('Object Repository/search_product_page/search_result_list'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/homepage/card_product'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/search_product_page/image_product_list'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/search_product_page/name_product_list'), 0)
	}


	@When("user tap category name elektronik")
	public void user_tap_category_name_elektronik() {
		Mobile.tap(findTestObject('Object Repository/homepage/button_category_elektronik'), 0)
	}

	@Then("the system will display a list of products with categories elektronik")
	public void the_system_will_display_a_list_of_products_with_categories_elektrronik() {
		Mobile.verifyElementVisible(findTestObject('Object Repository/homepage/card_view1'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/homepage/category_product'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/homepage/image_product_fx'), 0)
	}

	@Given("user already to homepage page as a guest")
	public void user_already_to_homepage_page_as_a_guest() {
		Mobile.startApplication('Application/secondhand-24082023.apk', true)
		Mobile.delay(3)
		Mobile.tap(findTestObject('Object Repository/homepage/button_akun'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/my_account/button_masuk'), 0)
		Mobile.tap(findTestObject('Object Repository/homepage/button_beranda'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/homepage/search_field'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/homepage/button_category_elektronik'), 0)
	}
}
