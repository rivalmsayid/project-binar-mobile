package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable
import cucumber.api.java.en.Given
import cucumber.api.java.en.When
import cucumber.api.java.en.Then
import org.openqa.selenium.Keys as Keys


public class notification_menu {

	@Given("user successfully log in and already on homepage")
	public void user_successfully_log_in_and_already_on_homepage() {
		Mobile.startApplication('Application/secondhand-24082023.apk', true)
		Mobile.delay(3)
		Mobile.tap(findTestObject('Object Repository/homepage/button_akun'), 0)
		Mobile.tap(findTestObject('Object Repository/login/button_masuk'), 0)
		Mobile.waitForElementPresent(findTestObject('Object Repository/login/input_email'), 0)
		Mobile.waitForElementPresent(findTestObject('Object Repository/login/input_password'), 0)
		Mobile.waitForElementPresent(findTestObject('Object Repository/login/button_masuk'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/login/input_email'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/login/input_password'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/login/button_masuk'), 0)
		Mobile.setText(findTestObject('Object Repository/login/input_email'), 'rendy123@gmail.com', 0)
		Mobile.setText(findTestObject('Object Repository/login/input_password'), '123456', 0)
		Mobile.tap(findTestObject('Object Repository/login/button_hide_password'), 0)
		Mobile.tap(findTestObject('Object Repository/login/button_masuk'), 0)
		Mobile.delay(3)
		Mobile.tap(findTestObject('Object Repository/homepage/button_beranda'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/homepage/search_field'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/homepage/button_category_elektronik'), 0)
	}

	@When("user tap menu notifikasi on bottom bar dashboard")
	public void user_tap_menu_notifikasi_on_bottom_bar_dashboard() {
		Mobile.tap(findTestObject('Object Repository/homepage/button_notifikasi'), 0)
	}

	@Then("the system will display a list of notifications received by the user")
	public void the_system_will_display_a_list_of_notifications_received_by_the_user() {
		Mobile.delay(3)
		Mobile.waitForElementPresent(findTestObject('Object Repository/notification_menu/text_notif_menu'), 0)
		Mobile.waitForElementPresent(findTestObject('Object Repository/notification_menu/card_notif'), 0)
		Mobile.waitForElementPresent(findTestObject('Object Repository/notification_menu/detail_message'), 0)
		Mobile.waitForElementPresent(findTestObject('Object Repository/notification_menu/detail_time'), 0)

		Mobile.verifyElementVisible(findTestObject('Object Repository/notification_menu/text_notif_menu'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/notification_menu/card_notif'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/notification_menu/detail_message'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/notification_menu/detail_time'), 0)
	}
}

