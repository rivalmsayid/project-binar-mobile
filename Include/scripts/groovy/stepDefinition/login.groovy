package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable
import cucumber.api.java.en.Given
import cucumber.api.java.en.When
import cucumber.api.java.en.Then

public class login {

	@Given("user is already on the login page")
	public void user_is_already_on_the_login_page() {
		Mobile.startApplication('Application/secondhand-24082023.apk', true)
		Mobile.tap(findTestObject('Object Repository/homepage/button_akun'), 0)
		Mobile.tap(findTestObject('Object Repository/my_account/button_masuk'), 0)
	}

	@When("user input valid credentials and click login button")
	public void user_input_valid_credentials_and_click_login_button() {
		Mobile.setText(findTestObject('Object Repository/login/input_email'), 'rendy123@gmail.com', 0)
		Mobile.setText(findTestObject('Object Repository/login/input_password'), '123456', 0)
		Mobile.tap(findTestObject('Object Repository/login/button_masuk'), 0)
	}

	@When("user input (.*) in login page")
	public void user_input_in_login_page(String condition) {

		if (condition == "invalid credentials") {
			Mobile.setText(findTestObject('Object Repository/login/input_email'), '123456@gmail.com', 0)
			Mobile.setText(findTestObject('Object Repository/login/input_password'), '654321', 0)
		}
		else if (condition == "empty email address and valid password") {
			Mobile.setText(findTestObject('Object Repository/login/input_email'),'', 0)
			Mobile.setText(findTestObject('Object Repository/login/input_password'), '123456', 0)
		}
		else if (condition == "valid email address and empty password") {
			Mobile.setText(findTestObject('Object Repository/login/input_email'), 'rendy123@gmail.com', 0)
			Mobile.setText(findTestObject('Object Repository/login/input_password'), '', 0)
		}
	}

	@Then("user has successfully login and is redirected to the account page")
	public void user_has_successfully_login_and_is_redirected_to_the_account_page() {
		Mobile.verifyElementVisible(findTestObject('Object Repository/homepage/button_akun'), 0)
	}

	@When("user click login button")
	public void user_click_login_button() {
		Mobile.tap(findTestObject('Object Repository/login/button_masuk'), 0)
	}

	@Then("user get pop up (.*)")
	public void user_get_pop_up(String result) {

		if(result == "Incorrect email or password") {
			Mobile.verifyElementVisible(findTestObject('Object Repository/login/button_masuk'), 0)
		}
		else if(result == "Email cannot be empty") {
			Mobile.verifyElementVisible(findTestObject('Object Repository/login/button_masuk'), 0)
		}
		else if(result == "Password cannot be empty") {
			Mobile.verifyElementVisible(findTestObject('Object Repository/login/button_masuk'), 0)
		}
	}
}

