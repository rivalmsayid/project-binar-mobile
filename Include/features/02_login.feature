@Login
Feature: login

  Scenario: TC.Log.002.001 - user has successfully logged in with valid information
    Given user is already on the login page
    When user input valid credentials and click login button
    Then user has successfully login and is redirected to the account page

   Scenario Outline: user cannot login
   	Given user is already on the login page
   	When user input <condition> in login page
   	And user click login button
   	Then user get pop up <result>
   	
   	Examples:
   	|   case_id    |               condition              |         result            | 
   	|TC.Log.002.002|           invalid credentials        |incorrect email or password|
   	|TC.Log.002.003|empty email address and valid password|email cannot be empty      |
   	|TC.Log.002.004|valid email address and empty password|password cannot be empty   |