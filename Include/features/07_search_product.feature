@SearchProduct
Feature: Search Product

  Scenario: TC-Sch-001 User want searches for available items in the system
    Given user successfully log in and already on homepage
    When user tap search field
    And user inputs the keyword buku in the search field
    Then the system will display a list of products with name buku

  Scenario: TC-Sch-002 User wants to find product based on category
    Given user successfully log in and already on homepage
    When user tap category name elektronik
    Then the system will display a list of products with categories elektronik

  Scenario: TC-Sch-003 user wants to search for items by not log in
    Given user already to homepage page as a guest
    When user tap search field
    And user inputs the keyword buku in the search field
    Then the system will display a list of products with name buku
