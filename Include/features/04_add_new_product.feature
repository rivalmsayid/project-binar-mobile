@AddNewProduct
Feature: add new product

  Background: TC.Log.002.001 - user has successfully logged in with valid information
    Given user is already on the login page
    When user input valid credentials and click login button
    Then user has successfully login and is redirected to the account page

  Scenario Outline: user is not succesfully add new product data because not fill <condition> field
    Given user in account page
    And user tap jual button
    And user redirect to add product page
    When user is input the product data except <condition> field
    And user tap terbitkan button
    Then user will not sucessfully add the product and see <message>

    Examples: 
      | case_id          | condition   | message                             |
      | TCM.Prod.001.002 | Name        | Nama Produk tidak boleh kosong      |
      | TCM.Prod.001.003 | Price       | Harga Produk tidak boleh kosong     |

  Scenario: TCM.Prod.001.001 user is succesfully add new product data
    Given user in account page
    And user tap jual button
    And user redirect to add product page
    When user is input all the product data fields
    And user tap terbitkan button
    Then user successfully add new product
