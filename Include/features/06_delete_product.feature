@DeleteProduct
Feature: Delete Product

  Scenario: TC-Delete.Product-001 User wants to delete product
    Given user successfully log in and already on akun menu
    When user tap menu daftar jual saya
    And user on already to sub menu produk
    And user tap symbol recycle bin on product
    And user select and tap hapus
    Then the system will delete the product and display allert produk berhasil dihapus
