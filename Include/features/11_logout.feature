@Logout
Feature: logout

  Scenario: TC.Log.002.001 - user has successfully logged in with valid information
    Given user is already on the login page
    When user input valid credentials and click login button
    Then user has successfully login and is redirected to the account page

  Scenario: TC.Log.002.001 - user want to logout
    When user click keluar button 
    Then user successfully log out
