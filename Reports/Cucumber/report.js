$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Include/features/07_search_product.feature");
formatter.feature({
  "name": "Search Product",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@SearchProduct"
    }
  ]
});
formatter.scenario({
  "name": "TC-Sch-001 User want searches for available items in the system",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@SearchProduct"
    }
  ]
});
formatter.step({
  "name": "user successfully log in and already on homepage",
  "keyword": "Given "
});
formatter.match({
  "location": "notification_menu.user_successfully_log_in_and_already_on_homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user tap search field",
  "keyword": "When "
});
formatter.match({
  "location": "search_product.user_tap_search_field()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user inputs the keyword buku in the search field",
  "keyword": "And "
});
formatter.match({
  "location": "search_product.user_inputs_the_keyword_buku_in_the_search_field()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the system will display a list of products with name buku",
  "keyword": "Then "
});
formatter.match({
  "location": "search_product.the_system_will_display_a_list_of_products_with_name_buku()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "TC-Sch-002 User wants to find product based on category",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@SearchProduct"
    }
  ]
});
formatter.step({
  "name": "user successfully log in and already on homepage",
  "keyword": "Given "
});
formatter.match({
  "location": "notification_menu.user_successfully_log_in_and_already_on_homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user tap category name elektronik",
  "keyword": "When "
});
formatter.match({
  "location": "search_product.user_tap_category_name_elektronik()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the system will display a list of products with categories elektronik",
  "keyword": "Then "
});
formatter.match({
  "location": "search_product.the_system_will_display_a_list_of_products_with_categories_elektrronik()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "TC-Sch-003 user wants to search for items by not log in",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@SearchProduct"
    }
  ]
});
formatter.step({
  "name": "user already to homepage page as a guest",
  "keyword": "Given "
});
formatter.match({
  "location": "search_product.user_already_to_homepage_page_as_a_guest()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user tap search field",
  "keyword": "When "
});
formatter.match({
  "location": "search_product.user_tap_search_field()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user inputs the keyword buku in the search field",
  "keyword": "And "
});
formatter.match({
  "location": "search_product.user_inputs_the_keyword_buku_in_the_search_field()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the system will display a list of products with name buku",
  "keyword": "Then "
});
formatter.match({
  "location": "search_product.the_system_will_display_a_list_of_products_with_name_buku()"
});
formatter.result({
  "status": "passed"
});
formatter.uri("Include/features/08_buy_product.feature");
formatter.feature({
  "name": "buy product",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@BuyProduct"
    }
  ]
});
formatter.scenario({
  "name": "TS.Buy.001.001 user want to buy product after login",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@BuyProduct"
    }
  ]
});
formatter.step({
  "name": "user login with valid credentials",
  "keyword": "Given "
});
formatter.match({
  "location": "buy_product.user_login_with_valid_credentials()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user go to homepage",
  "keyword": "When "
});
formatter.match({
  "location": "buy_product.user_go_to_homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user choose the product that want to buy",
  "keyword": "And "
});
formatter.match({
  "location": "buy_product.user_choose_the_product_that_want_to_buy()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user submit offering product",
  "keyword": "And "
});
formatter.match({
  "location": "buy_product.user_submit_offering()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user successfully buy the product",
  "keyword": "Then "
});
formatter.match({
  "location": "buy_product.user_successfully_buy_product()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "TS.Buy.001.002 user want to buy product before login",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@BuyProduct"
    }
  ]
});
formatter.step({
  "name": "user already on homepage",
  "keyword": "Given "
});
formatter.match({
  "location": "buy_product.user_already_on_homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user choose the product",
  "keyword": "And "
});
formatter.match({
  "location": "buy_product.user_choose_the_product()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click offer button",
  "keyword": "And "
});
formatter.match({
  "location": "buy_product.user_click_offer_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user can see alert message that user must login first",
  "keyword": "Then "
});
formatter.match({
  "location": "buy_product.user_can_see_alert_message()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "TS.Buy.001.003 user want to buy product with 0 on the offer page",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@BuyProduct"
    }
  ]
});
formatter.step({
  "name": "user already login",
  "keyword": "Given "
});
formatter.match({
  "location": "buy_product.user_already_login()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user choose the product to buy",
  "keyword": "When "
});
formatter.match({
  "location": "buy_product.user_choose_the_product_to_buy()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input the nominal of product offer with 0 and click submit",
  "keyword": "And "
});
formatter.match({
  "location": "buy_product.user_input_the_nominal_of_product()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user stay in the product page",
  "keyword": "Then "
});
formatter.match({
  "location": "buy_product.user_stay_in_the_product_page()"
});
formatter.result({
  "status": "passed"
});
formatter.uri("Include/features/09_notification.feature");
formatter.feature({
  "name": "Notification",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Notification"
    }
  ]
});
formatter.scenario({
  "name": "TC-Notif-001 user wants to access the notification menu",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Notification"
    }
  ]
});
formatter.step({
  "name": "user successfully log in and already on homepage",
  "keyword": "Given "
});
formatter.match({
  "location": "notification_menu.user_successfully_log_in_and_already_on_homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user tap menu notifikasi on bottom bar dashboard",
  "keyword": "When "
});
formatter.match({
  "location": "notification_menu.user_tap_menu_notifikasi_on_bottom_bar_dashboard()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the system will display a list of notifications received by the user",
  "keyword": "Then "
});
formatter.match({
  "location": "notification_menu.the_system_will_display_a_list_of_notifications_received_by_the_user()"
});
formatter.result({
  "status": "passed"
});
formatter.uri("Include/features/10_sell_product.feature");
formatter.feature({
  "name": "sell product",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@SellProduct"
    }
  ]
});
formatter.scenario({
  "name": "TS.Sell.001.001 user want to sell product after login",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@SellProduct"
    }
  ]
});
formatter.step({
  "name": "user login as seller",
  "keyword": "Given "
});
formatter.match({
  "location": "sell_product.user_login_as_seller()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user view the notification",
  "keyword": "When "
});
formatter.match({
  "location": "sell_product.user_view_the_notification()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user choose product offer",
  "keyword": "And "
});
formatter.match({
  "location": "sell_product.user_choose_product_offer()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user accept the offer from buyer",
  "keyword": "And "
});
formatter.match({
  "location": "sell_product.user_accept_offer()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user stay on the offer page",
  "keyword": "Then "
});
formatter.match({
  "location": "sell_product.user_stay_on_the_offer_page()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "TS.Sell.001.002 user want to sell product before login",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@SellProduct"
    }
  ]
});
formatter.step({
  "name": "user go to home page",
  "keyword": "Given "
});
formatter.match({
  "location": "sell_product.user_go_to_homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click the sell button",
  "keyword": "When "
});
formatter.match({
  "location": "sell_product.user_click_the_sell_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user redirected to login page that user must login first",
  "keyword": "Then "
});
formatter.match({
  "location": "sell_product.user_redirected_to_login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.uri("Include/features/11_logout.feature");
formatter.feature({
  "name": "logout",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Logout"
    }
  ]
});
formatter.scenario({
  "name": "TC.Log.002.001 - user has successfully logged in with valid information",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Logout"
    }
  ]
});
formatter.step({
  "name": "user is already on the login page",
  "keyword": "Given "
});
formatter.match({
  "location": "login.user_is_already_on_the_login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input valid credentials and click login button",
  "keyword": "When "
});
formatter.match({
  "location": "login.user_input_valid_credentials_and_click_login_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user has successfully login and is redirected to the account page",
  "keyword": "Then "
});
formatter.match({
  "location": "login.user_has_successfully_login_and_is_redirected_to_the_account_page()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "TC.Log.002.001 - user want to logout",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Logout"
    }
  ]
});
formatter.step({
  "name": "user click keluar button",
  "keyword": "When "
});
formatter.match({
  "location": "logout.user_click_keluar_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user successfully log out",
  "keyword": "Then "
});
formatter.match({
  "location": "logout.user_successfully_log_out()"
});
formatter.result({
  "status": "passed"
});
});