package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import java.security.PublicKey

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable
import cucumber.api.java.en.Given
import cucumber.api.java.en.When
import cucumber.api.java.en.Then

public class register {

	@Given("user is already on the registration page")
	public void user_is_already_on_the_registration_page() {
		Mobile.startApplication('Application/secondhand-24082023.apk', true)

		Mobile.tap(findTestObject('Object Repository/homepage/button_akun'), 0)

		Mobile.tap(findTestObject('Object Repository/my_account/button_masuk'), 0)

		Mobile.tap(findTestObject('Object Repository/login/button_daftar'), 0)
	}

	@When("user input valid information and click register button")
	public void user_input_valid_information_and_click_register_button() {
		Mobile.setText(findTestObject('Object Repository/register/input_username'), 'tester11', 0)

		Mobile.setText(findTestObject('Object Repository/register/input_email'), 'tester11@gmail.com', 0)

		Mobile.setText(findTestObject('Object Repository/register/input_password'), 'tester11', 0)

		Mobile.setText(findTestObject('Object Repository/register/input_no_hp'), '082180808094', 0)

		Mobile.setText(findTestObject('Object Repository/register/input_kota'), 'Cimahi', 0)

		Mobile.setText(findTestObject('Object Repository/register/input_alamat'), 'Jawa Barat', 0)

		Mobile.tap(findTestObject('Object Repository/register/button_daftar'), 0)
	}

	@Then("user has successfully registers and is redirected to the account page")
	public void user_has_successfully_registers_and_is_redirected_to_the_account_page() {
		Mobile.verifyElementVisible(findTestObject('Object Repository/homepage/button_akun'), 0)
	}
}
