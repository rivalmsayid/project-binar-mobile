package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import java.sql.Driver

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.mobile.keyword.internal.MobileAbstractKeyword
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.testobject.MobileTestObject.MobileLocatorStrategy
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import ch.qos.logback.core.joran.conditional.ElseAction
import cucumber.api.java.en.Given
import cucumber.api.java.en.When
import cucumber.api.java.en.Then
import internal.GlobalVariable

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import io.appium.java_client.AppiumDriver

import com.kms.katalon.core.util.KeywordUtil


public class buy_product {
	@Given('user login with valid credentials')
	public void user_login_with_valid_credentials() {
		Mobile.startApplication('Application/secondhand-24082023.apk', true)
		Mobile.tap(findTestObject('Object Repository/homepage/button_akun'), 0)
		Mobile.tap(findTestObject('Object Repository/my_account/button_masuk'), 0)
		Mobile.setText(findTestObject('Object Repository/login/input_email'), 'tester11@gmail.com', 0)
		Mobile.setText(findTestObject('Object Repository/login/input_password'), 'tester11', 0)
		Mobile.tap(findTestObject('Object Repository/login/button_masuk'), 0)
	}

	@When('user go to homepage')
	public void user_go_to_homepage() {
		Mobile.tap(findTestObject('Object Repository/buy_product/button_beranda'), 0)
	}

	@When('user choose the product that want to buy')
	public void user_choose_the_product_that_want_to_buy() {
		Mobile.tap(findTestObject('Object Repository/homepage/search_field'), 0)
		Mobile.delay(3)
		Mobile.tap(findTestObject('Object Repository/search_product_page/search field page'), 0)
		Mobile.setText(findTestObject('Object Repository/search_product_page/search field page'), 'botol aqua 1', 0)
		Mobile.tap(findTestObject('Object Repository/buy_product/card_product_search1'), 0)
	}

	@When('user submit offering product')
	public void user_submit_offering() {
		Mobile.tap(findTestObject('Object Repository/buy_product/button_saya_tertarik'), 0)
		Mobile.setText(findTestObject('Object Repository/buy_product/input_harga_tawar'), '30000', 0)
		Mobile.tap(findTestObject('Object Repository/buy_product/button_kirim_penawaran'), 0)
	}

	@Then('user successfully buy the product')
	public void user_successfully_buy_product() {
		AppiumDriver<?> driver = MobileDriverFactory.getDriver()
		driver.findElementsByXPath("//android.widget.Toast[@text='Harga tawarmu berhasil dikirim ke penjual']")
		Mobile.verifyElementVisible(findTestObject('Object Repository/buy_product/button_menunggu_respon_penjual'), 0)
		Mobile.delay(5)
	}

	@Given('user already on homepage')
	public void user_already_on_homepage() {
		Mobile.startApplication('Application/secondhand-24082023.apk', true)
		Mobile.verifyElementVisible(findTestObject('Object Repository/buy_product/card_product_view'), 0)
	}

	@When('user choose the product')
	public void user_choose_the_product() {
		Mobile.tap(findTestObject('Object Repository/buy_product/card_product_view'), 0)
	}

	@When('user click offer button')
	public void user_click_offer_button() {
		Mobile.tap(findTestObject('Object Repository/buy_product/button_saya_tertarik'), 0)
	}

	@Then('user can see alert message that user must login first')
	public void user_can_see_alert_message() {
		AppiumDriver<?> driver = MobileDriverFactory.getDriver()
		driver.findElementsByXPath("//android.widget.Toast[@text='Silakan login terlebih dahulu']")
		Mobile.verifyElementVisible(findTestObject('Object Repository/login/button_masuk'), 0)
		Mobile.delay(5)
	}

	@Given('user already login')
	public void user_already_login() {
		Mobile.startApplication('Application/secondhand-24082023.apk', true)
		Mobile.tap(findTestObject('Object Repository/homepage/button_akun'), 0)
		Mobile.tap(findTestObject('Object Repository/my_account/button_masuk'), 0)
		Mobile.setText(findTestObject('Object Repository/login/input_email'), 'tester11@gmail.com', 0)
		Mobile.setText(findTestObject('Object Repository/login/input_password'), 'tester11', 0)
		Mobile.tap(findTestObject('Object Repository/login/button_masuk'), 0)
		Mobile.tap(findTestObject('Object Repository/homepage/button_beranda'), 0)
	}

	@When('user choose the product to buy')
	public void user_choose_the_product_to_buy() {
		Mobile.tap(findTestObject('Object Repository/homepage/search_field'), 0)
		Mobile.delay(3)
		Mobile.tap(findTestObject('Object Repository/search_product_page/search field page'), 0)
		Mobile.setText(findTestObject('Object Repository/search_product_page/search field page'), 'botol aqua 2', 0)
		Mobile.tap(findTestObject('Object Repository/buy_product/card_product_search2'), 0)
	}

	@When('user input the nominal of product offer with 0 and click submit')
	public void user_input_the_nominal_of_product() {
		Mobile.tap(findTestObject('Object Repository/buy_product/button_saya_tertarik'), 0)
		Mobile.setText(findTestObject('Object Repository/buy_product/input_harga_tawar'), '0', 0)
		Mobile.tap(findTestObject('Object Repository/buy_product/button_kirim_penawaran'), 0)
	}

	@Then('user stay in the product page')
	public void user_stay_in_the_product_page() {
		AppiumDriver<?> driver = MobileDriverFactory.getDriver()
		driver.findElementsByXPath("//android.widget.Toast[@text='Harga tawarmu berhasil dikirim ke penjual']")
		Mobile.verifyElementVisible(findTestObject('Object Repository/buy_product/button_menunggu_respon_penjual'), 0)
		Mobile.delay(5)
	}
}
